output "instance1_ip" {
  value = linode_instance.instance1.ipv4
}

output "instance1_private_ip" {
  description = "Private ip of instance 1"
  value = linode_instance.instance1.private_ip
}

output "instance2_ip" {
  value = linode_instance.instance2.ipv4
}

output "instance2_private_ip" {
  description = "Private ip of instance 2"
  value = linode_instance.instance2.private_ip
}