# app.py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, this is a simple Flask app deployed with CapRover!'
